const express = require('express');
const app = express();
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;
const password = 'dota';

app.get('/:word', (req, res) => {
    res.send(req.params.word);
});



app.get('/encode/:word', (req, res) => {
    const result = Vigenere.Cipher(password).crypt(req.params.word);
    res.send(result);
});

app.get('/decode/:word', (req, res) => {
    const result = Vigenere.Decipher(password).crypt(req.params.word);
    res.send(result);
});

app.listen(port, () => {
    console.log('we are live on ' + port);
});